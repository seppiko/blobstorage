/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.blobstorage.utils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.imageio.ImageIO;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Leonard Woo
 */
@Slf4j
public class ImageUtil {

  public static boolean convertImageType (InputStream is, OutputStream os, String targetFormat) {
    try{
      BufferedImage image = ImageIO.read(is);
      return ImageIO.write(image, targetFormat, os);
    } catch (IOException ex) {
      log.warn("Image type convert failed.", ex);
    }
    return false;
  }
}
