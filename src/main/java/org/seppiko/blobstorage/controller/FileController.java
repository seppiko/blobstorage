/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.blobstorage.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import lombok.extern.slf4j.Slf4j;
import org.seppiko.blobstorage.models.MessageEntity;
import org.seppiko.blobstorage.utils.HashUtil;
import org.seppiko.blobstorage.utils.ImageUtil;
import org.seppiko.blobstorage.utils.JsonUtil;
import org.seppiko.blobstorage.utils.ResponseUtil;
import org.seppiko.blobstorage.utils.S3Util;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Leonard Woo
 */
@Slf4j
@RestController
public class FileController {

  private boolean flag = false;

  @RequestMapping(value = "/upload", method = RequestMethod.POST)
  public ResponseEntity<byte[]> uploadContentHandleExecution(@RequestParam("file") MultipartFile file) {
    try {
      if (!typeCheck(file.getContentType())) {
        return ResponseUtil.sendResponseContent(HttpStatus.BAD_REQUEST, MediaType.APPLICATION_JSON,
            JsonUtil.toJson(
                new MessageEntity(HttpStatus.BAD_REQUEST.value(), "File type disallow"))
                .getBytes());
      }
      if( file.isEmpty() ) {
        throw new FileNotFoundException();
      }
      byte[] bytes = file.getBytes();

      if (!flag) {
        OutputStream os = new ByteArrayOutputStream(bytes.length);
        ImageUtil.convertImageType(new ByteArrayInputStream(bytes), os, "jpg");
        os.flush();
        os.write(bytes);
        os.close();
      }

      String name = HashUtil.inputStreamHash(bytes);
      name = name + ".jpg";
      S3Util.getInstance().saveFile(bytes, name);
      return ResponseUtil.sendResponseContent(HttpStatus.OK, MediaType.APPLICATION_JSON,
          JsonUtil.toJson(
              new MessageEntity(HttpStatus.OK.value(), "/bs/" + name))
              .getBytes());
    } catch (IOException e) {
      log.error("File upload failed.", e);
    }
    return ResponseUtil.sendResponseContent(HttpStatus.OK, MediaType.APPLICATION_JSON, "{}".getBytes());
  }

  private boolean typeCheck (String type) {
    if ( MediaType.IMAGE_JPEG_VALUE.equalsIgnoreCase(type) ) {
      flag = true;
    } else if ( MediaType.IMAGE_PNG_VALUE.equalsIgnoreCase(type) ) {
    } else {
      return false;
    }
    return true;
  }

  @RequestMapping(value = "/bs/{name}", method = RequestMethod.GET)
  public ResponseEntity<byte[]> downloadContentHandleExecution(@PathVariable("name") String name) {
    byte[] result = S3Util.getInstance().loadFile(name);
    if(result == null) {
      return ResponseUtil.sendResponseContent(HttpStatus.OK, MediaType.APPLICATION_JSON,
          JsonUtil.toJson(
              new MessageEntity(HttpStatus.NOT_FOUND.value(), "file not found.")).getBytes());
    }
    return ResponseUtil.sendResponseContent(HttpStatus.OK, MediaType.IMAGE_JPEG, result);
  }
}
