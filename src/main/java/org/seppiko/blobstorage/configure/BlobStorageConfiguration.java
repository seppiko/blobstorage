/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.blobstorage.configure;

/**
 * @author Leonard Woo
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BlobStorageConfiguration {

  private static BlobStorageConfiguration instance = new BlobStorageConfiguration();
  public static BlobStorageConfiguration getInstance() {
    return instance;
  }
  private BlobStorageConfiguration() {
    init();
  }

  private final String CONFIGURATION_FILE = "configFile";

  @Getter
  private String url;

  @Getter
  private String accessKey;

  @Getter
  private String secretKey;

  @Getter
  private String bucket;

  @Getter
  private String regions;

  private void init() {
    try {
      String path = System.getProperty(CONFIGURATION_FILE);
//      log.info("default path: " + path);
      File file;
      if(path != null) {
        file = new File(path);
      } else {
        file = new File("config.yaml");
      }
      log.debug(file.getAbsolutePath());
      if (!file.exists()) {
        throw new RuntimeException("Load config failed.");
      }
      ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
      JsonNode blobstorageJson = mapper.readTree(getStream(file)).get("blobstorage");

      this.url = blobstorageJson.get("url").asText();
      this.regions = blobstorageJson.get("regions").asText();
      this.accessKey = blobstorageJson.get("accessKey").asText();
      this.secretKey = blobstorageJson.get("secretKey").asText();
      this.bucket = blobstorageJson.get("bucket").asText();

    } catch (Exception e) {
      log.error("Load config failed.", e);
    }
  }

  @Override
  public String toString() {
    return "BlobStorageConfiguration{" +
        "url='" + url + '\'' +
        ", accessKey='" + accessKey + '\'' +
        ", secretKey='" + secretKey + '\'' +
        ", bucket='" + bucket + '\'' +
        '}';
  }

  private InputStream getStream(File file) throws FileNotFoundException {
    return new BufferedInputStream(new FileInputStream(file));
  }
}
